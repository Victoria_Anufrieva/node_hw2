const {Router: router} = require('express');
const {register, login} = require('../controllers/auth');
const authRouter = router();

authRouter.post('/register', register);
authRouter.post('/login', login);

module.exports = authRouter;
