const fs = require('fs');
require('dotenv').config();
const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const noteRouter = require('./routes/note');
const authRouter = require('./routes/auth');
const auth = require('./middlewares/auth');
const userRouter = require('./routes/user');
const app = express();
const PORT = 8080;

app.use(express.json());
app.use(cors());
app.use(function(req, res, next) {
  const log = [new Date().toLocaleString(),
    req.ip, req.originalUrl, req.method];
  fs.appendFile('logs.log', log.join(', ') + '\n', () => {});
  next();
});

app.use('/api/notes', [auth, noteRouter]);
app.use('/api/users', [auth, userRouter]);
app.use('/api/auth', authRouter);

mongoose.connect(
    `mongodb+srv://${process.env.DB_LOGIN}:${process.env.DB_PWD}@cluster0.jbzkx.mongodb.net/myFirstDatabase?retryWrites=true&w=majority`,
);

app.listen(PORT, function() {
  console.log(`Web server listening on port ${PORT}`);
});
